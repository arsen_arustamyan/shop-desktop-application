﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class AuthService
    {
        private string email { set; get; }
        private string password { set; get; }

        public AuthService(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        public async Task<string> Auth(string url)
        {
            WebRequest request = WebRequest.Create($"{url}?email={this.email}&password={this.password}");
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    } 
                }
            }
        }
    }
}
