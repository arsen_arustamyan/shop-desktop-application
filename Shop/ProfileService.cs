﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class ProfileService
    {
        public async Task<string> GetCountProduct(string url, string login)
        {
            WebRequest request = WebRequest.Create(url + "?login=" + login);
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }

        public async Task<string> GetPrice(string url, string login)
        {
            WebRequest request = WebRequest.Create(url + "?login=" + login);
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }
    }
}
