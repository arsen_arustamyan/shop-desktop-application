﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class CartService
    {
        public async Task<string> AddProductCart(string url, string id, string login)
        {
            WebRequest request = WebRequest.Create($"{url}?id={id}&login={login}");
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }
        public async Task<string> GetProductsCart(string url, string login)
        {
            WebRequest request = WebRequest.Create($"{url}?login={login}");
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        return await sr.ReadToEndAsync();
                    }
                }
            }
        }
    }
}
