﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop
{
    /// <summary>
    /// Логика взаимодействия для CartWindow.xaml
    /// </summary>
    public partial class CartWindow : Window
    {
        private string login;
        private List<CartProduct> products = new List<CartProduct>();

        public CartWindow(string login)
        {
            InitializeComponent();
            this.login = login;

            getProducts();
        }

        public async void getProducts()
        {
            var cartService = new CartService();
            Task<string> task = cartService.GetProductsCart(MyApplication.path + "webapp/api/getProductsInCart.php", login);
            string responseServer = await task;

            products = JsonConvert.DeserializeObject<List<CartProduct>>(responseServer);
            productsCart.ItemsSource = products;
        }
    }
}
