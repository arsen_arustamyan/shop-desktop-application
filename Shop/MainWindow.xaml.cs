﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Shop
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AuthService authService = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void authButton_Click(object sender, RoutedEventArgs e)
        {
            var email = emailInput.Text;
            var password = passwordInput.Text;

            authService = new AuthService(email, password);
            Task<string> task = authService.Auth(MyApplication.path + "webapp/api/auth.php");
            string responseServer = await task;

            var result = JsonConvert.DeserializeAnonymousType(responseServer, new { response = "" });

            if (result.response == "1")
                { new ShopIndex(email).Show(); Close(); }
            else
                MessageBox.Show("Неправильный логин/пароль", "Произошла ошибка авторизации");
        }
    }
}
