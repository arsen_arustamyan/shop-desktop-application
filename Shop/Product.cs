﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Product
    {
        public string id { get; set; }
        public string name { get; set; }
        //public string description { get; set; }
        public string path { get; set; }
        public string cena { get; set; }

        public Product(string id, string name, string path, int cena)
        {
            this.id = id;
            this.name = name;
            //this.description = description.Substring(0, 100) + "...";
            this.path = MyApplication.path + path;
            this.cena = cena + " руб.";
        }
    }
}
