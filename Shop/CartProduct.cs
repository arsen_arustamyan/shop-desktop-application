﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class CartProduct : Product
    {
        public int count_product { get; set; }

        public CartProduct(string id, string name, string path, int cena, int count_product) : base(id, name, path, cena)
        {
            this.count_product = count_product;
        }
    }
}
