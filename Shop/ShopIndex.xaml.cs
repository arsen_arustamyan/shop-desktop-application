﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop
{
    /// <summary>
    /// Логика взаимодействия для ShopIndex.xaml
    /// </summary>
    public partial class ShopIndex : Window
    {
        private string login = null;
        private List<Product> products = new List<Product>();
        private string countProductsUser = null;
        private string priceAllProducts = null;

        public ShopIndex(string login)
        {

            InitializeComponent();
            this.login = login;

            GetCountProductsInCart(this.login);

            GetPrice(this.login);

            InitialProductsList();

        }

        private async void addCartBtn_Click(object sender, RoutedEventArgs e)
        {

            Button btn = (Button) sender;
            var cartService = new CartService();
            Task<string> task = cartService.AddProductCart(MyApplication.path + "webapp/api/addProductCart.php", btn.Tag.ToString(), login);
            string responseServer = await task;

            var response = JsonConvert.DeserializeAnonymousType(responseServer, new { response = "" });

            if (response.response == "true")
            {
                var currentCount = countProductsInCart.Text.Split(' ')[0];
                int newCount = int.Parse(currentCount);
                newCount++;
                countProductsInCart.Text = $"{newCount} предмет(а)";

                GetPrice(this.login);
            }
            else
            {
                MessageBox.Show("Ошибка, статус: 500");
            }
        }

        private async void InitialProductsList()
        {
            string productsJSON = null;
            WebRequest request = WebRequest.Create(MyApplication.path + "webapp/api/products.php");
            Task<WebResponse> webResponse = request.GetResponseAsync();

            using (var response = await webResponse)
            {
                using (var stream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        Task<string> task = sr.ReadToEndAsync();
                        productsJSON = await task;
                    }
                }
            }

            alertContainer.Visibility = Visibility.Hidden;

            products = JsonConvert.DeserializeObject<List<Product>>(productsJSON);
            ordersList.ItemsSource = products;
        }

        private async void GetCountProductsInCart(string login)
        {
            var profileService = new ProfileService();
            Task<string> task = profileService.GetCountProduct(MyApplication.path + "webapp/api/getCount.php", login);
            string responseServer = await task;

            countProductsUser = JsonConvert.DeserializeAnonymousType(responseServer, new { count = "" }).count;

            countProductsInCart.Text = $"{countProductsUser} предмет(а)";
        }

        private async void GetPrice(string login)
        {
            var profileService = new ProfileService();
            Task<string> task = profileService.GetPrice(MyApplication.path + "webapp/api/getPrice.php", login);
            string responseServer = await task;

            var response = (new { price = "" });
            priceAllProducts = JsonConvert.DeserializeAnonymousType(responseServer, new { price = "" }).price;

            priceBlock.Text = $"{priceAllProducts} руб.";
        }

        private void cartContainerClick(object sender, MouseButtonEventArgs e)
        {
            new CartWindow(login).Show();
        }
    }
}
